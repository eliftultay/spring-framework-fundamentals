//All  Configuration Begins Here

import com.eliftultay.repository.HibernateSpeakerRepositoryImpl;
import com.eliftultay.repository.SpeakerRepository;
import com.eliftultay.service.SpeakerService;
import com.eliftultay.service.SpeakerServiceImpl;
import com.eliftultay.util.CalendarFactory;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;

import java.util.Calendar;

@Configuration
@ComponentScan({"com.eliftultay"})
public class AppConfig {

    @Bean(name = "cal")
    public CalendarFactory calendarFactory(){
        CalendarFactory factory = new CalendarFactory();
        factory.addDays(2);
        return factory;
    }

    @Bean
    public Calendar cal() throws Exception{
        return calendarFactory().getObject();
    }

    /*
   @Bean(name = "speakerService")
   @Scope(value = BeanDefinition.SCOPE_SINGLETON)
   //@Scope(value = BeanDefinition.SCOPE_PROTOTYPE)
    public SpeakerService getSpeakerService(){

       SpeakerServiceImpl service = new SpeakerServiceImpl();

       //Contructor injection
       //SpeakerServiceImpl service = new SpeakerServiceImpl(getSpeakerRepository());

       //Setter injection
       //service.setRepository(getSpeakerRepository());

       return service;
    }*/

    /*
    @Bean(name = "speakerRepository")
    public SpeakerRepository getSpeakerRepository() {
        return new HibernateSpeakerRepositoryImpl();
    }*/
}
