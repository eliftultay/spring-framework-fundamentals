package com.eliftultay.repository;

import com.eliftultay.model.Speaker;

import java.util.List;

public interface SpeakerRepository {
    List<Speaker> findAll();
}
