package com.eliftultay.service;

import com.eliftultay.model.Speaker;
import com.eliftultay.repository.HibernateSpeakerRepositoryImpl;
import com.eliftultay.repository.SpeakerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.List;

@Service("speakerService")
@Profile("dev")
public class SpeakerServiceImpl implements SpeakerService {

    private SpeakerRepository repository ;

    public SpeakerServiceImpl(){
        System.out.println("SpeakerServiceImpl no args constructor.");
    }

    //Constructor injection
    public SpeakerServiceImpl (SpeakerRepository speakerRepository){
        System.out.println("SpeakerServiceImpl repository constructor.");
        repository = speakerRepository;
    }

    @PostConstruct
    private void initialize(){
        System.out.println("We are called for constructors!");
    }

    @Override
    public List<Speaker> findAll() {
        return repository.findAll();
    }

    //Setter injection
   @Autowired
    public void setRepository(SpeakerRepository repository) {
        System.out.println("SpeakerServiceImpl setter.");
        this.repository = repository;
    }
}
