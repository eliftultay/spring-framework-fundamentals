package com.eliftultay.service;

import com.eliftultay.model.Speaker;

import java.util.List;

public interface SpeakerService {
    List<Speaker> findAll();
}
